# README #

Speculator is a small self contained web server present software specifications
and the status of unit tests covering those specifications.

## Install

Use `composer require gcdtech/speculator`

## Usage

> Note these examples assume your bin-dir composer property is set to "bin/". If yours differs
> be sure to change the path to speculator.

Invoke speculator using the terminal:

``` bash
bin/speculator [pathtospecs] [pathtotests] [bootstrap-php-file]
```

Or create a speculator.json file in the same folder e.g.

``` javascript
{
  "specs-dir": "specs",
  "tests-dir": "tests/unit",
  "bootstrap": "tests/_bootstrap.php"
}
```

And this invoke in a simpler way:

``` bash
bin/speculator
```

## Specification Documents

Specification documents are simply markdown files that live in a folder normally called `specs`.

`index.md` should exist as an root entry page.

Create links between your pages in the normal markdown way.

Inject a unit test assessment using the following syntax:

```
(@Path/To/Unit/Test::testMethodName)
```

If this exists inside a bullet item then the bullet style will change to a tick or an x depending on the test success.
Otherwise a tick or x will be dropped in place of the test marker.
 
## Extended markdown syntax

This project uses the [erusev/parsedown-extra](https://github.com/erusev/parsedown-extra)
parser for markdown which provides a range of additional markdown goodies.

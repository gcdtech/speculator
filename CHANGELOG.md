# Changelog

### 1.0.3

* Fixed: 	Move phpunit to require-dev

### 1.0.2

* Added:	New support for integration, unit and ui behaviours
* Added:	Facility to remove test insertions
* Changed:	Specs now appear at the start of the line again

### 1.0.1

* Changed:	Specs now appear at their insertion point
* Added:	Images are now passed through to the normal web server
* Added:	Support for (@todo) 

### 1.0.0

* Added:        First version

<?php

namespace Gcd\Speculator;

use NiJobFinder\Application\Exceptions\Exception;
use PHPUnit_Framework_TestResult;
use PHPUnit_Framework_TestSuite;
use PHPUnit_TextUI_TestRunner;

$settings = [
    "specs-dir" => "specs",
    "tests-dir" => "tests"
];

if (file_exists("speculator.json")){
    $json = json_decode(file_get_contents("speculator.json"), true);

    $settings = array_merge($settings, $json);
}

$specsEnv = getenv("SPECSDIR");

if ($specsEnv){
    $settings["specs-dir"] = $specsEnv;
}

$testsEnv = getenv("TESTSDIR");
if ($testsEnv) {
    $settings["tests-dir"] = $testsEnv;
}

$testsDir = $settings["tests-dir"];
$specsDir = $settings["specs-dir"];

$bootstrap = getenv("BOOTSTRAP");
if ($bootstrap){
    $settings["bootstrap"] = $bootstrap;
}

if (isset($settings["bootstrap"]) && $settings["bootstrap"]){
    include_once $settings["bootstrap"];
}

include_once __DIR__."/../../../autoload.php";

$test = (isset($_GET["test"])) ? $_GET["test"] : false;

if ($test){

    sem_acquire(sem_get(123, 1));

    $parts = explode("::", $test);
    $file = $parts[0];

    $class = basename($file);

    $phpunit = new PHPUnit_TextUI_TestRunner;

    if (!file_exists($testsDir."/".$file.".php")){
        print json_encode(["result" => false, "error" => "Test class does not exist"]);
        exit;
    }

    try {
        /**
         * @var PHPUnit_Framework_TestSuite $test
         */
        $test = $phpunit->getTest($class, $testsDir."/".$file.".php", ".php");

        /** @var \PHPUnit_Framework_Test $realTest */
        $realTest = null;
        foreach($test->tests() as $methodTest){
            if ($methodTest->getName() == $parts[1]){
                $realTest = $methodTest;
                break;
            }
        }

        if (!$realTest){
            throw new \InvalidArgumentException();
        }

        $realTest->setInIsolation(true);
        /** @var PHPUnit_Framework_TestResult $result */
        $result = $realTest->run();

        $success = ($result->errorCount() == 0 && count($result->passed()) > 0);

        $json = ["result" => $success];

        if ($result->errorCount() > 0){
            foreach($result->errors() as $error){
                $json["error"] = $error->toString();
            }
        }

        if ($result->failureCount() > 0){
            foreach($result->failures() as $error){
                $json["error"] = $error->toString();
            }
        }

        print json_encode($json);
        exit;
    } catch (\Throwable $e) {
        print json_encode(["result" => false]);
        exit;
    }
}

$uitest = (isset($_GET["uitest"])) ? $_GET["uitest"] : false;
if($uitest){
    $parts = explode("::", $uitest);
    $testClassName = $parts[0];
    if(array_key_exists(1, $parts))
        $function = $parts[1];

    $bootstrapFile = realpath("$testsDir/..") . "/_bootstrap.php";
    if(!file_exists($bootstrapFile))
        throw new Exception("Error bootstrapping rhubarb");
    include_once $bootstrapFile;

    require_once realpath("$testsDir/../acceptance/") . "/$testClassName.php";
    $testClassName = basename($testClassName);

    $testObject = new $testClassName();

    $testReflection = new \ReflectionClass($testClassName);
    $results = [];
    foreach ($testReflection->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
        if(strpos($method->name, "mochaTest") !== false){
            $name = $method->name;
            $result = $testObject->$name();
            echo json_encode(array('results' => $result, 'class' => $testClassName));
            exit;
        }
    }
}

if (defined("SPECS_URL_STUB")) {
    $_SERVER["REQUEST_URI"] = preg_replace('/^' . str_replace("/", "\\/", SPECS_URL_STUB) . '/', '/', $_SERVER["REQUEST_URI"]);
} else {
    define("SPECS_URL_STUB", "/");
}

if ($_SERVER["REQUEST_URI"] == "/"){
    $_SERVER["REQUEST_URI"] = "/index.md";
}

$uri = $_SERVER["REQUEST_URI"];

if (preg_match('/\.(?:png|jpg|jpeg|gif)$/', $_SERVER["REQUEST_URI"])) {
    $size = getimagesize($specsDir.$_SERVER["REQUEST_URI"]);
    header("Content-type: ".$size['mime']);
    readfile($specsDir.$_SERVER["REQUEST_URI"]);
    exit;
}

if (preg_match('/\.(?:xls|xlsx|csv|docx)$/', $_SERVER["REQUEST_URI"])) {
    header("Content-type: application/octet-stream");
    readfile($specsDir.$_SERVER["REQUEST_URI"]);
    exit;
}

$glossary = [];

// Is there a glossary?
if (file_exists($specsDir."/glossary.md")){

    $glossaryLines = file($specsDir."/glossary.md");
    $recording = false;
    $currentHeading = "";
    $capturedLines = "";

    for($x = 0; $x < count($glossaryLines); $x++){
        $line = $glossaryLines[$x];

        if ($line[0]=="#"){
            continue;
        }

        if (($x+1 < count($glossaryLines))
            && $glossaryLines[$x+1][0]==":"){
            if ($recording){
                $glossary[$currentHeading] = $capturedLines;
            }

            $currentHeading = trim($line);

            $recording = true;
            $capturedLines = preg_replace("/^:/", "", trim($line)). " ";
        } else if ($recording) {
            $capturedLines .= trim($line)." ";
        }
    }

    if ($recording){
        $glossary[$currentHeading] = $capturedLines;
    }
}

$file = file_get_contents($specsDir.$uri);

$parser = new \ParsedownExtra();

$html = $parser->parse($file);

$html = preg_replace( "/href=\"\\/([^\"]+)\"/", 'href="'.SPECS_URL_STUB.'\1"', $html);
$html = preg_replace("/\\(@todo\\)/i", "<span class=\"test todo\"></span>", $html);
$html = preg_replace("/\\(\\*todo\\)/i", "<span class=\"test integration todo\"></span>", $html);
$html = preg_replace("/\\(#ui\\)/i", "<span class=\"ui\"></span>", $html);
$html = preg_replace("/\\(@([^)]+)\\)/", "<span class=\"test unit\" data-test=\"\\1\">&nbsp;</span>", $html);
$html = preg_replace("/\\(#([^)]+)\\)/", "<span class=\"test ui\" data-test=\"\\1\">&nbsp;</span>", $html);
$html = preg_replace("/\\(\\*([^)]+)\\)/", "<span class=\"test integration\" data-test=\"\\1\">&nbsp;</span>", $html);

foreach($glossary as $phrase => $value){
    $html = preg_replace('/([>\s])'.$phrase.'/', "\\1--".$phrase."--", $html);
}

$t = $html;

while(preg_match('/--([^-]+)--/', $t, $match)){
    $html = str_replace($match[0], '<span class="glossary" title="'.htmlentities($glossary[$match[1]]).'">'.$match[1].'</span>', $html);
    $t = str_replace($match[0], "", $t);
}

$html .= <<<END
<script>

var showTestsCheckbox = document.getElementById('showTests');

function toggleTests(){
    if (showTestsCheckbox.checked){
        document.body.classList.add('tests');
    } else {
        document.body.classList.remove('tests');
    }
}

showTestsCheckbox.addEventListener('click',function(){
    toggleTests();
});

toggleTests();

var spans = document.querySelectorAll('span.ui');

for(var s = 0; s < spans.length; s++){
    var span = spans[s];
    span.parentNode.className = "test ui";
}

spans = document.querySelectorAll('span.test');

for(var s = 0; s < spans.length; s++){
    var span = spans[s];
    span.parentNode.className = span.className + " testing";
    
    if (span.classList.contains('todo')){
        span.parentNode.title = 'Todo';
        continue;
    }
    
    span.parentNode.setAttribute('title', span.getAttribute('data-test'));
}


if (spans.length>0){
    
    function nextTest(sn){
        if (sn >= spans.length){
            return;
        }        
        
        var span = spans[sn];
        if(span.parentNode.classList.contains('passed') || span.parentNode.classList.contains('failed')){
            nextTest(sn + 1);
            return;
        }
        
        var isUiTest = span.classList.contains('ui');
        
        if (span.classList.contains('todo')){
            span.parentNode.title = 'Todo';
            nextTest(sn + 1);
            return;
        }
        
        var xhr = new XMLHttpRequest();
        xhr.open("GET", window.location.href + '?' + (isUiTest ? 'uitest' : 'test') + '=' + span.getAttribute('data-test'), true);
        xhr.setRequestHeader('X-Requested-With', 'xmlhttprequest');
        
        xhr.onreadystatechange = function (xhr, span) {
            return function(){
                if (xhr.readyState == 4) {
                    if (xhr.responseText != null) {
                        var result = JSON.parse(xhr.responseText);
                        
                        if(isUiTest){
                            for(var i = 0; i < spans.length; i++){
                                var test = spans[i].getAttribute('data-test');
                                for(var j = 0; j < result.results.length; j++){
                                    if(test.indexOf(result['class'] + "::" + result['results'][j]['test']) !== -1){
                                        spans[i].parentNode.className += (result['results'][j]['result'] == 'pass') ? " passed" : " failed";
                                    }
                                }
                            }
                        }else{
                            span.parentNode.className += (result.result) ? " passed" : " failed";
                            span.innerHTML = span.getAttribute('data-test');
                            
                            if (!result.result){
                                span.parentNode.setAttribute('title', result.error);
                            }
                        }
                        
                        
                        nextTest(sn+1);
                    }
                }
            }
        }(xhr, span);
        
        xhr.send();    
    }
    
    nextTest(0);    
}

</script>
END;

$css = file_get_contents(__DIR__.'/css/main.css');

$html = <<<END
<html>

    <head>
        <style>
            $css
        </style>
    </head>
    
    <body>
        <div class="o-wrap">
            <div class="s-main-content o-box">
            <div id="showTestsContainer">
            <p><input type="checkbox" id="showTests" checked>Show Tests</p>
            
            <div class="legend" style="display:none">
                <div class="key test integration"></div> Integration<br/>
                <div class="key test unit"></div> Unit<br/>
                <div class="key behaviour"></div> UI Behaviour
            </div>
            
            </div>
                $html
            </div>
        </div>
    </body>

</html>
END;

print $html;
